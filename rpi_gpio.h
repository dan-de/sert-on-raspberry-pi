#ifndef _RASPBERRYPI_H_
#error You must not include this sub-header file directly
#endif

#define GPIO_BASE       0x20200000

iomemdef(GPIO_GPFSEL4, 	GPIO_BASE + 0x10);
iomemdef(GPIO_GPSET1, 	GPIO_BASE + 0x20);
iomemdef(GPIO_GPCLR1, 	GPIO_BASE + 0x2c);
iomemdef(GPIO_GPLEV1, 	GPIO_BASE + 0x38);

//#define gpio_state_mask(N,V) ((iomem(GPIO_GPFSEL4) >> N) & V)
#define gpio_set_output_mask(V) do{ \
	iomem(GPIO_GPFSEL4) = (V); }while(0)
#define gpio_on_mask(V) do{ \
	iomem(GPIO_GPSET1) = (V); }while(0)
#define gpio_off_mask(V) do{ \
	iomem(GPIO_GPCLR1) = (V); }while(0)


//#define gpio_state(N,V)	gpio_state_mask(N,V)
#define gpio_set_output(V)	gpio_set_output_mask(1 << V)
#define gpio_on(V)			gpio_on_mask(1 << V)
#define gpio_off(V)			gpio_off_mask(1 << V)

