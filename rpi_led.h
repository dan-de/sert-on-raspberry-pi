#ifndef _RASPBERRYPI_H_
#error You must not include this sub-header file directly
#endif

#define led_on				gpio_on(15)
#define led_off				gpio_off(15)
#define led_gpio_set_output	gpio_set_output(21)
