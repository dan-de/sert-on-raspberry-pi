#ifndef _RASPBERRYPI_H_
#error You must not include this sub-header file directly
#endif

extern void main(void);

static inline
void loop_delay(unsigned long d)
{
	while (d-- > 0)
		__memory_barrier();
}
