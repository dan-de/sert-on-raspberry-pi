#ifndef _RASPBERRYPI_H_
#define _RASPBERRYPI_H_

typedef unsigned int u32;

static volatile u32 *const _iomem = (u32 *) 0;
#define iomemdef(N, V) enum { N = (V) / sizeof(u32) }
#define iomem(N) _iomem[N]

static inline void iomem_high(unsigned int reg, u32 mask)
{
		iomem(reg) |= mask;
}
static inline void iomem_low(unsigned int reg, u32 mask)
{
		iomem(reg) &= ~mask;
}

#include "rpi_cpu.h"
#include "rpi_gpio.h"
#include "rpi_led.h"
#include "comm.h"

#endif
