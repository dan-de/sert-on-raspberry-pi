#include "raspberrypi.h"

static void fill_bss(void)
{
	extern u32 _bss_start, _bss_end;
	u32 *p;

	for (p = &_bss_start; p < &_bss_end; ++p)
		*p = 0UL;
}

void _init(void)
{
	fill_bss();
	main();
}
