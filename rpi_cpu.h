#ifndef _RASPBERRYPI_H_
#error You must include this sub-header file directly
#endif

/* ARM1176JZF-S Tech Manual, p. 3-85 */

#define __memory_barrier() \
	__asm__ __volatile__ ("MCR p15,0,%[dummy],c7,c10,5": : [dummy] "r" (0) : "memory")
