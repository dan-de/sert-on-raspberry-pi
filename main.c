#include "raspberrypi.h"

void init_gpio(void)
{
	led_gpio_set_output;
} 
 
/** Main function - we'll never return from here */
void main(void)
{
    init_gpio();    
    while(1)
    {
		if(!((iomem(GPIO_GPLEV1) >> 15) & 1))
			led_on;
		else
			led_off;			
		loop_delay(1000000);
    }
}
